/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 10/11/15
 * @file pcLoaderNodelet.cpp
 * @brief Nodelet for loading a point cloud or mesh file and publishing it as a PointCloud2 message.
 */


// project
#include <pcLoader.hpp>

// dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <pf_matching_tools/loaderConfig.h>

// ROS code
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

// ROS messages
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/String.h>
#include <geometry_msgs/TransformStamped.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>

// other
#include <string>



namespace pf_matching{
namespace ROS{
    
/**
 * @class PCloadSettings
 * @brief Class for reading and storing the node's settings
 */
class PCloadSettings{
    
    public:
        
        // ROS topics
        std::string pathTopic;      //!< ROS topic name for dynamic path infos
        std::string publishTopic;   //!< ROS topic name to publish the loaded cloud to
        std::string transformTopic; //!< inverse of transformations applied to the cloud before publishing
        
        // other parameters
        std::string initialPath; //!< first path to load from, "" for none

        /**
         * @brief Read the node settings from the private node handle.
         * @param privateNh private node handle
         * @return true if reading was successfull, else false
         */
        bool readSettings(const ros::NodeHandle& privateNh){
            
            // get topics
            privateNh.param("path_topic", pathTopic, std::string("file_load_path"));
            privateNh.param("publish_topic", publishTopic, std::string("point_cloud_raw"));
            privateNh.param("transform_topic", transformTopic, std::string("applied_transforms"));
            
            // get possible first path
            privateNh.param("path", initialPath, std::string(""));
            
            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;
            
        }
    private:
        /**
         * @brief Print out a nice help message on how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for PCloaderNodelet\n"
                << "-------------------------------------\n"
                << "(To hide this message, start the node / nodelet with _help:=false)\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "path_topic:      topic with path of files to load and publish\n"
                << "publish_topic:   topic to publish loaded clouds to\n"
                << "transform_topic: topic to publish the inverse to the transformation applied to "
                << "the cloud before publishing (i.e. appling it to the published cloud will restore "
                << "the original data); NOTE: Does not include any scaling operations!\n"
                << "path:          initial path to load from, if empty (default), nothing "
                << "will be published initially.\n"
                << "\n"
                << "Further parameters are available through the dynamic_reconfigure interface.\n"
                << "\n"
                );
        }
};


/**
 * @brief Nodelet for loading a point cloud or mesh file and publishing it as a pointCloud2 message.
 */
class PCloaderNodelet : public nodelet::Nodelet{
    
    public:
        
        //! levels of dynamic reconfigure parameters, exact clone of values set in cfg/loader.cfg
        enum dynReconfigureLevel : uint32_t{
            AFFECTS_LOAD_AND_PROCESS = 1, //!< parameters affect the loading and processing of clouds
            AFFECTS_PUBLISH_SETUP    = 2, //!< parameters affect the publishing
            };
        
    private:
    
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // general node handle, manager's namespace, usually for topic access
            nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters

            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }
            
            // get initial path
            path = settings.initialPath;
            
            // publishers
            pubPC = nh.advertise<sensor_msgs::PointCloud2>(settings.publishTopic, 10, true);
            pubTransform = nh.advertise<geometry_msgs::TransformStamped>(settings.transformTopic,
                                                                         10, true);
            
            // in rare cases, publishers might return empty
            if (!pubPC || ! pubTransform){
                NODELET_ERROR_STREAM("nh.advertise returned an empty publisher. Shutting down.");
                exit(EXIT_FAILURE);
            }
            
            // subscriber to path topic
            subPath = nh.subscribe(settings.pathTopic, 10, &PCloaderNodelet::newPathCallback,
                                   this);
                                   
            // set up dynamic reconfigure callback
            dynReconfigServer.setCallback(boost::bind(&PCloaderNodelet::dynamicReconfigureCallback,
                                                      this, _1, _2));
        }
        
        /**
         * @brief handling of a new path message
         * @param message_in the received ROS message
         */
        void newPathCallback(const std_msgs::String::ConstPtr& message_in){

            NODELET_DEBUG_STREAM("Got new path to load from: " << message_in->data);
            
            // save the parameters given through the message
            path = message_in->data;
            
            // load and process the point cloud
            if(!loadAndProcessPC()){
                
                if(!path.empty()){
                    NODELET_INFO_STREAM("Point cloud cloud not be loaded. Waiting for next message.");
                }
                
                return;
            }
            
            // publish the cloud if no timer callback will do so at a rate
            if(config.publish_rate == 0){
                publishPC(); // publish regardless of if there is a subscriber or not
            }
        }
    
        
        /**
         * @brief Load a point cloud from the file path specified by the settings and store it in the 
         *        given point cloud.
         * @return true if loading was successfull, else false
         */
        bool loadAndProcessPC(){
            
            appliedTransformInverse = Eigen::Affine3f::Identity();

            if(!got_initial_config){
                NODELET_ERROR("Got no dynamic reconfigure paramters. Not loading.");
                return false;
            }
        
            // if the path is empty, return an empty cloud
            if(path.empty()){
                cloudPtr->clear();
                return false;
            }
            
            // create a loader object and load the point cloud
            io::PCloader<pcl::PointNormal> loader(cloudPtr);
            bool loadOK = loader.loadCloud(path);
        
            // if the loading failed, we can skip the rest
            if(!loadOK){
                path.clear(); // path did not make sense, remember that for future loading attempts
                return false;
            }
            
            // rescale the cloud if requested
            if(config.scale_factor != 1.0){
                loader.scaleCloud(config.scale_factor);
                NODELET_DEBUG_STREAM("Point cloud rescaled.");
            }
            
            // recenter the cloud if requested
            if(config.recenter_cloud){
                Eigen::Vector3f recenterTranslation = loader.recenterCloud();
                appliedTransformInverse *= Eigen::Translation3f(recenterTranslation);
                NODELET_DEBUG_STREAM("Point cloud recentered.");
            }
            
            return true;
        }
        
        /**
         * @brief Publish a point cloud as a pointCloud2 message with the frame_id specified by the 
         *        node's config.

         * The point cloud data is copied to a newly created message at every function call to comply
         * with nodelet standards.
         */
        void publishPC(){

            if(!got_initial_config){
                NODELET_ERROR("Got no dynamic reconfigure paramters. Not publishing.");
                return;
            }
            
            // create a new point cloud message, fill it, publish it
            sensor_msgs::PointCloud2::Ptr msg(new sensor_msgs::PointCloud2);
            
            pcl::toROSMsg(*cloudPtr, *msg);         // copy point cloud data
            msg->header.stamp = ros::Time::now();   // add current time
            msg->header.frame_id = config.frame_id; // add sensor frame_id
            
            pubPC.publish(msg);
            NODELET_INFO_STREAM("Point cloud published to topic '" + pubPC.getTopic() + "'."
                                << " points: " << cloudPtr->points.size()
                                << " frame_id: " << config.frame_id);


            // create a new transformation message
            geometry_msgs::TransformStamped::Ptr transformMsg(new  geometry_msgs::TransformStamped);
            
            // fill it with the inverse of the applied transformation
            Eigen::Quaternionf q(appliedTransformInverse.rotation());
            Eigen::Vector3f t = appliedTransformInverse.translation();
            
            transformMsg->transform.translation.x = t.x();
            transformMsg->transform.translation.y = t.y();
            transformMsg->transform.translation.z = t.z();

            transformMsg->transform.rotation.x = q.x();
            transformMsg->transform.rotation.y = q.y();
            transformMsg->transform.rotation.z = q.z();
            transformMsg->transform.rotation.w = q.w();
            
            transformMsg->header.stamp = msg->header.stamp;
            transformMsg->header.frame_id = config.frame_id;
            transformMsg->child_frame_id = config.frame_id + "_original";
            
            // publish transformation message
            pubTransform.publish(transformMsg);
        }
        
        /**
         * @brief publishing function run at every timer callback, checks for subscribers before
         *        actually publishing
         * @param timerEvent unused but required by ROS
         */
        void publishViaTimer(const ros::TimerEvent& timerEvent){
            
            if(pubPC.getNumSubscribers() > 0u && !cloudPtr->points.empty()){
                publishPC();
            }
            else if(pubPC.getNumSubscribers() == 0u) {
                NODELET_INFO("There are currently no subscribers. Cloud was not published.");
            }
        }
    
        /**
         * @brief callback for changing dynamic reconfigure parameters, copies the parameters in
         *        the appropriate place
         * @param config_in the new configuration
         * @param level bitfield specifying which parameters changed
         */
        void dynamicReconfigureCallback(pf_matching_tools::loaderConfig &config_in, uint32_t level){

            NODELET_DEBUG_STREAM("Got new dynamic reconfigure configuration. Level: " << level);
            
            // copy parameters and remember init
            float oldPublishRate = config.publish_rate;
            config = config_in;
            got_initial_config = true;
            
            // if the new parameters affect the publishing rate and it was changed
            if(level & AFFECTS_PUBLISH_SETUP && oldPublishRate != config.publish_rate){

                if(config.publish_rate == 0){
                    
                    // just publish once -> stop timer if it was running before
                    timer.stop();
                }
                else{
                    // publish at a rate -> new timer callback or replace old one
                    timer = nh.createTimer(ros::Duration(ros::Rate(config.publish_rate)),
                                           &PCloaderNodelet::publishViaTimer,
                                           this);
                }
            }
            
            /*
             * Not used anymore, settings relating to the loading and processing only affect
             * future loading operations.
             * 
            // if the new parameters affect how the cloud is loaded and processed,
            // we need to reload and republish it
            if(level & AFFECTS_LOAD_AND_PROCESS){
                
                // trigger loading via a "fake" message with the currently set path
                std_msgs::String::Ptr fakeMsg(new std_msgs::String);
                fakeMsg->data = path;
                newPathCallback(fakeMsg);
            }
             * */
        }
        
        // ROS related
        ros::NodeHandle nh;          //!< public node handle
        ros::Publisher pubPC;        //!< publisher for the loaded point cloud
        ros::Publisher pubTransform; //!< publisher for the applied transforms
        ros::Timer timer;            //!< timer for publishing at a rate
        ros::Subscriber subPath;     //!< subscriber to the topic with the load path of the point cloud
        
        // various others
        PCloadSettings settings; //!< static node settings
        pcl::PointCloud<pcl::PointNormal>::Ptr cloudPtr =       //!< pointer to the cloud for loading
                        pcl::PointCloud<pcl::PointNormal>::Ptr(new pcl::PointCloud<pcl::PointNormal>);
        std::string path; //!< load path, empty means no loading is to take place
        Eigen::Affine3f appliedTransformInverse; //!< inverse of transforms done before publishing
        
        // dynamic reconfigure
        dynamic_reconfigure::Server<pf_matching_tools::loaderConfig> dynReconfigServer; //!<
        pf_matching_tools::loaderConfig config; //!< latest dynamic reconfigure parameters
        bool got_initial_config = false;        //!< prevents processing with uninitialized parameters
};


} // end namespace
} // end namespace

// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::PCloaderNodelet, nodelet::Nodelet)