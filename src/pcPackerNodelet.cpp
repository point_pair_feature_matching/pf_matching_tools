/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 17/05/16
 * @file PcpackerNodelet.cpp
 * @brief Nodelet for unpacking the sufrace and edge cloud of a processed clouds message to two
 *        separate topics.
 */



// ROS code
#include <ros/ros.h>
#include <nodelet/nodelet.h>             // for nodelets
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages
#include <sensor_msgs/PointCloud2.h>
#include <pf_matching_core/ProcessedClouds.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>

// other
#include <string>
#include <random>

namespace pf_matching{
namespace ROS{

/**

 * @brief Class for reading and storing the node's settings.
 */
class PCpackerSettings{
    public:

        
        std::string output_processed_clouds; //!< the (packed) processed cloud
        std::string input_surface;         //!< topic of the surface part
        std::string input_edge ;           //!< topic of the edge part
    
        /**
         * @brief Read the node settings from the private node handle.
         * @param privateNh private node handle
         * @return true if reading was successfull, else false
         */
        bool readSettings(ros::NodeHandle& privateNh){
            
            // get topics
            privateNh.param("output_processed_clouds", output_processed_clouds, std::string("processed_clouds"));
            privateNh.param("input_surface",         input_surface,         std::string("surface"));
            privateNh.param("input_edge",            input_edge,            std::string("edge"));
            
            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;
        }
        
        /**
         * @brief Print a nice help message about how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for PcpackerNodelet\n"
                << "-------------------------------------\n"
                << "This nodelet takes individual edge and sufrace messages and publishes them as a "
                << "ProcessedClouds message.\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "output_processed_clouds: topic to publish the processed clouds message to\n"
                << "input_surface:         topic of surface part\n"
                << "input_edge:            topic of edge part\n"
                << "\n"
                << "(To hide this message, start the node / nodelet with _help:=false)"
                );
        }
};




/**
 * @brief Nodelet for unpacking and republishing a processd clouds message
 */
class PCpackerNodelet : public nodelet::Nodelet{
    
    public:
        
    private:
    
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters
            
            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }
            
            // publishers
            pubPC = nh.advertise<pf_matching_core::ProcessedClouds>(settings.output_processed_clouds, 10, true);
            if (!pubPC){
                NODELET_ERROR_STREAM("nh.advertise returned an empty publisher. Shutting down.");
                exit(EXIT_FAILURE);
            }

            // subscribers
            subSurface = nh.subscribe(settings.input_surface, 10, &PCpackerNodelet::save_surface, this);
            subEdge = nh.subscribe(settings.input_edge, 10, &PCpackerNodelet::save_edge, this);

        }
        

        void save_surface(const sensor_msgs::PointCloud2::ConstPtr& msg_in){
            surfacePtr = msg_in;
            packAndPublish();
        }
        
        void save_edge(const sensor_msgs::PointCloud2::ConstPtr& msg_in){
            edgePtr = msg_in;
            packAndPublish();
        }
        
        /**
         * @brief pack surface and edge into a ProcessedClouds message and publish them if both 
         *        were received earlier
         */
        void packAndPublish(){
            
            if(surfacePtr && edgePtr){
                
                pf_matching_core::ProcessedClouds::Ptr msg_out(new pf_matching_core::ProcessedClouds);
                msg_out->surface = *surfacePtr;
                msg_out->edges   = *edgePtr;
                
                pubPC.publish(msg_out);
                
                surfacePtr = NULL;
                edgePtr = NULL;
            }
        }
   
    // ROS related
    ros::Publisher pubPC;       //!< publisher for packed message
    ros::Subscriber subSurface; //!< subscriber for surface clouds
    ros::Subscriber subEdge;    //!< subscriber for edge clouds
    
    // clouds storage
    sensor_msgs::PointCloud2::ConstPtr surfacePtr;
    sensor_msgs::PointCloud2::ConstPtr edgePtr;
    
    // settings
    PCpackerSettings settings; //!< topic settings
};

} // end namespace
} // end namespace


// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::PCpackerNodelet, nodelet::Nodelet)