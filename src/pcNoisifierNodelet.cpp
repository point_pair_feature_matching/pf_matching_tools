/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 10/11/15
 * @file pcNoisifierNode.cpp
 * @brief Nodelet for adding zero mean gaussian white noise to a ROS pointCloud2 message and 
 *        republishing it.
 */


// project
#include <boundingSphere.hpp>
#include <pcNoisifier.hpp>
#include <nodeletWithStatistics.hpp>

// dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <pf_matching_tools/noisifierConfig.h>

// ROS code
#include <ros/ros.h>
#include <nodelet/nodelet.h>             // for nodelets
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages
#include <sensor_msgs/PointCloud2.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>

// other
#include <string>
#include <random>

namespace pf_matching{
namespace ROS{

/**
 * @class PCnoisifySettings
 * @brief Class for reading and storing the node's settings.
 */
class PCnoisifySettings{
    public:

        
        std::string input_topic_original;    //!< ROS topic of original cloud
        std::string output_topic_noisy;      //!< ROS topic for publishing noisy cloud
        std::string output_topic_statistics; //!< general statistics topic
    
        /**
         * @brief Read the node settings from the private node handle.
         * @param privateNh private node handle
         * @return true if reading was successfull, else false
         */
        bool readSettings(ros::NodeHandle& privateNh){
            
            // get topics
            privateNh.param("input_topic_original",    input_topic_original,    std::string("point_cloud_raw"));
            privateNh.param("output_topic_noisy",      output_topic_noisy,      std::string("point_cloud_noisy"));
            privateNh.param("output_topic_statistics", output_topic_statistics, std::string("pf_statistics"));
            
            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;
        }
        
        /**
         * @brief Print a nice help message about how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for PCnoisifierNodelet\n"
                << "-------------------------------------\n"
                << "This nodelet adds gaussian white noise to a point cloud and republishes it.\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "input_topic_original:    topic of the original point clouds\n"
                << "output_topic_noisy:      topic to publish noisy clouds to\n"
                << "output_topic_statistics: topic publishing general statistics about node operation\n"
                << "\n"
                << "Further parameters are available through the dynamic_reconfigure interface.\n"
                << "(To hide this message, start the node / nodelet with _help:=false)"
                );
        }
};




/**
 * @brief Nodelet for taking a point cloud message, adding zero mean
 *        gausian white noise to it and publishing it to ROS again.
 */
class PCnoisiferNodelet : public NodeletWithStatistics{
    
    public:
        typedef pcl::PointNormal                   internalPointType; //!< point type used in this nodelet
        typedef pcl::PointCloud<internalPointType> internalCloudType; //!< cloud type used in this nodelet
    
        /**
         * @brief Empty default constructor
         */
        PCnoisiferNodelet(){
            
        }
        
        
    private:
    
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters
            
            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }
            
            // publisher for noisy point cloud, in rare cases, Publisher might be empty
            pubPC = nh.advertise<sensor_msgs::PointCloud2>(settings.output_topic_noisy, 10, true);
            if (!pubPC){
                NODELET_ERROR_STREAM("nh.advertise returned an empty publisher. Shutting down.");
                exit(EXIT_FAILURE);
            }
            
            // publisher for statistics
            initStatisticsPublisher(nh, settings.output_topic_statistics);

            // subscriber for original point cloud
            subPC = nh.subscribe(settings.input_topic_original, 10,
                                 &PCnoisiferNodelet::noisifyPCandPublish, this);
            
            // set up dynamic reconfigure callback
            dynReconfigServer.setCallback(boost::bind(&PCnoisiferNodelet::dynamicReconfigureCallback,
                                                      this, _1, _2));
            
        }
        
        /**
         * @brief Take a point cloud message, add zero mean gaussian white noise to all points
         *        and re-publish the cloud
         * @param msg_in point cloud message passed by the subscriber
         */
        void noisifyPCandPublish(const sensor_msgs::PointCloud2ConstPtr& msg_in){
            
            // check that the dynamic reconfigure parameters initialized properly
            if(!got_initial_config){
                NODELET_ERROR("Got no dynamic reconfigure paramters. Doing nothing.");
                return;
            }

            // extract message content to local storage
            pcl::fromROSMsg(*msg_in, *cloudPtr);
            
            // add noise
            float absSD = noisifier.noisifyPC<internalPointType>(cloudPtr,
                                                                 config.noise_sd,
                                                                 config.sd_is_relative);

            // create a new message and fill it
            sensor_msgs::PointCloud2::Ptr msg_out(new sensor_msgs::PointCloud2);
            pcl::toROSMsg(*cloudPtr, *msg_out);
            msg_out->header = msg_in->header;   // we keep time and frame_id of the original message

            // publish message
            pubPC.publish(msg_out);
            NODELET_INFO_STREAM("Noisy point cloud published to topic '" + pubPC.getTopic() + "'.");
            
            // publish noise level as statistics
            publishStatistics("noise_sd_abs", absSD, true);
        }
        
        /**
         * @brief callback for changing dynamic reconfigure parameters, copies the parameters in
         *        the appropriate place
         * @param config_in the new configuration
         * @param level bitfield specifying which parameters changed
         */
        void dynamicReconfigureCallback(pf_matching_tools::noisifierConfig &config_in, uint32_t level){

            NODELET_DEBUG_STREAM("Got new dynamic reconfigure configuration.");
            
            // copy parameters and remember init
            config = config_in;
            got_initial_config = true;
            
            // set up noisifier according to config
            noisifier.setSeed(config.random_seed);
            noisifier.setMethod(static_cast<pre_processing::PCnoisifier::NoisificationMethod>(config.noisification_method));
        }
        
   
    // ROS related
    ros::Publisher pubPC;   //!< publisher for the noisy point cloud
    ros::Subscriber subPC;  //!< subscriber for the original point cloud
    
    // dynamic reconfigure
    dynamic_reconfigure::Server<pf_matching_tools::noisifierConfig> dynReconfigServer; //!<
    pf_matching_tools::noisifierConfig config; //!< latest dynamic reconfigure parameters
    bool got_initial_config = false;           //!< prevents processing with uninitialized parameters
    
    // various others
    PCnoisifySettings settings;
    internalCloudType::Ptr cloudPtr = internalCloudType::Ptr(new internalCloudType);
    pre_processing::PCnoisifier noisifier;

};

} // end namespace
} // end namespace

// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::PCnoisiferNodelet, nodelet::Nodelet)