/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ros/ros.h"
#include "nodelet/loader.h"
 
int main(int argc, char **argv){
    ros::init(argc, argv, "pcPacker");
    nodelet::Loader nodelet;
    nodelet::M_string remap(ros::names::getRemappings());
    nodelet::V_string nargv;
    std::string nodelet_name = ros::this_node::getName();
    nodelet.load(nodelet_name, "pf_matching_tools/PCpackerNodelet", remap, nargv);
    ros::spin();
    return 0;
}