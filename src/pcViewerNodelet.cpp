/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 10/11/15
 * @file pcViewerNodelet.cpp
 * @brief Nodelet for displaying point clouds and normals.
 */


// project
#include <pcViewer.hpp>
#include <resultVisualization.hpp>

// ROS code
#include <ros/ros.h>
#include <nodelet/nodelet.h>             // for nodelets
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages
#include <sensor_msgs/PointCloud2.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>

// other
#include <string>

#define POINT_SIZE 3


namespace pf_matching{
namespace ROS{

/**
 * @class cloudDisplaySettings
 * @brief storage for all settings required to display a point cloud
 */
class cloudDisplaySettings{
    public:
    
        int r;  //!< red color value
        int g;  //!< green color value
        int b;  //!< blue color value
        int pointSize = 1;      //!< display size of points
        std::string topicName;  //!< name of the topic the cloud originates from
    
    /**
     * @brief construct by copying values (see member variable description)
     */
    cloudDisplaySettings(std::string topicName_, int r_, int g_, int b_, int pointSize_){
        
        topicName = topicName_;
        r = r_;
        g = g_;
        b = b_;
        pointSize = pointSize_;
    }
};
    
    
/**
 * @class PCviewerSettings
 * @brief Class for reading and storing the node's settings.
 */
class PCviewerSettings{
    public:

        std::vector<cloudDisplaySettings> displaySettings;
        

        bool readSettings(ros::NodeHandle& privateNh){
            

            std::random_device rd; // obtain a random number from hardware
            std::mt19937 eng(rd()); // seed the generator
            std::uniform_int_distribution<> distr(0, 255); // define the range
            
            // find out how many topics there are 
            bool read_ok = true;
            int n = 0;
            while(read_ok){
                // try to read topic number i from parameter server
                std::string topicName("topic_" + std::to_string(n));
                std::string tmpTopic;
                read_ok = privateNh.getParam(topicName, tmpTopic);
                
                if(read_ok){
                    n++;
                }
            }
            
            ROS_INFO_STREAM("Got " << n << " input topic(s).");


            if(n>0){
                
                // add topics and assign a color
                int i = 0;
                read_ok = true;
                while(read_ok){
                    
                    // try to read topic number i from parameter server
                    std::string topicName("topic_" + std::to_string(i));
                    std::string tmpTopic;
                    read_ok = privateNh.getParam(topicName, tmpTopic);
                    
                    // create settings and store
                    if(read_ok){
                        
                        float r, g, b;
                        visualization::createRGBcolor(r, g, b, i, n);
                        displaySettings.push_back(cloudDisplaySettings(tmpTopic, r, g, b, POINT_SIZE));
                        i++;
                    }
                }
                if(i>0){
                    return true;
                }
            }
            else{
                printHelp();
                return false;
            }
        }
        
        /**
         * @brief Print a nice help message about how to use the node.
         */
        void printHelp(){
            ROS_WARN("\nCould not read all parameters successfully. Typical usage:\n"
                    "rosrun  pf_matching_research pcNoisifier <parameters>\n"
                    "mandatory parameters:\n"
                    "_topic_0:=/your_pointCloud2_topic\n"
                    "You can supply as many topics as you want, specifiy them in the form of:"
                    "topic_0, topic_1, ...\n");
        }
};


/**
 * @class PCviewerNodelet
 * @brief nodelet to display the point clouds of a number of ROS-topics
 */
class PCviewerNodelet : public nodelet::Nodelet{
    
    public:

        
    private:
    
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters
            
            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }
            
            // initialize data storage
            // note: Can't initialize vector of shared pointers via resize(). Doing so would fill
            // the vector with pointers to ONE object instead of creating a new one for each pointer.
            wasUpdated.resize(settings.displaySettings.size(), false);
            hasNormals.resize(settings.displaySettings.size(), false);
            for(int i = 0; i < settings.displaySettings.size(); i++){
                cloudPtrs.push_back(
                    pcl::PointCloud<pcl::PointNormal>::Ptr(new pcl::PointCloud<pcl::PointNormal>));
            }


            
            // create a subscriber for each given topic
            for(int i = 0; i < settings.displaySettings.size(); i++){
                subscribers.push_back(
                            nh.subscribe<sensor_msgs::PointCloud2>(
                                    settings.displaySettings[i].topicName, 10, boost::bind(& PCviewerNodelet::pcCallback, this, _1, i)));
            }
        
            // set up timer with callback
            timer = nh.createTimer(ros::Duration(0.05),
                                       &PCviewerNodelet::timerCallback,
                                       this);
        }
        
        /**
         * @brief callback for a new arriving point cloud
         * @param msg_in
         * @param topicNumber
         */
        void pcCallback(const sensor_msgs::PointCloud2ConstPtr& msg_in, int topicNumber){

            std::cout << "got new message on topic number " << topicNumber << "\n";
            
            // read in the data
            pcl::fromROSMsg(*msg_in, *cloudPtrs[topicNumber]);
            wasUpdated[topicNumber] = true;
            
            // remember if the cloud was XYZ only or also contains normals
            for(int i = 0; i < msg_in->fields.size(); i++){
                if(msg_in->fields[i].name.compare("normal_x") == 0){
                    hasNormals[topicNumber] = true;
                    break;
                }
            }
            
        }
        
        /**
         * @brief update the view every time a timer runs out
         * @param timerEvent (currently unused)
         */
        void timerCallback(const ros::TimerEvent& timerEvent){
            
            static visualization::PCviewer viewer(ros::this_node::getName());
            for(static bool first = true;first;first=false){
                // setup only executed once
                viewer.addOrigin(0.25);
            }
            
            // exit node if window was closed / q was pressed
            if(viewer.wasStopped()){
                exit(EXIT_SUCCESS);
            }
            
            // check if any new data arrived and if so, publish it
            for(int i = 0; i< settings.displaySettings.size(); i++){
                if(wasUpdated[i]){
                    wasUpdated[i] = false;
                    cloudDisplaySettings s = settings.displaySettings[i];
                    std::cout << "updating " << s.topicName << "\n";
                    if(hasNormals[i]){
                        viewer.addCloudNormal(cloudPtrs[i], s.topicName, s.r, s.g, s.b, s.pointSize);
                    }
                    else{
                        viewer.addCloud(cloudPtrs[i], s.topicName, s.r, s.g, s.b, s.pointSize);
                    }
                    
                }
                
            }
            
            // provess all viewer callbacks
            viewer.spinOnce(10, true);
        }
   
    // ROS related
    std::vector<ros::Subscriber> subscribers; // subscribers to point cloud topics
    std::vector<pcl::PointCloud<pcl::PointNormal>::Ptr> cloudPtrs; // received latest point clouds per topic
    std::vector<bool> wasUpdated; // element i indicates, whether the data in clouds[i] was updated by a subscriber callback
    std::vector<bool> hasNormals;
  
    // various others
    PCviewerSettings settings;
    ros::Timer timer; 


};

} // end namespace
} // end namespace


// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::PCviewerNodelet, nodelet::Nodelet)