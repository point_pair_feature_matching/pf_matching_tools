/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 17/05/16
 * @file pcUnpackerNodelet.cpp
 * @brief Nodelet for unpacking the sufrace and edge cloud of a processed clouds message to two
 *        separate topics.
 */



// ROS code
#include <ros/ros.h>
#include <nodelet/nodelet.h>             // for nodelets
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages
#include <sensor_msgs/PointCloud2.h>
#include <pf_matching_core/ProcessedClouds.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>

// other
#include <string>
#include <random>

namespace pf_matching{
namespace ROS{

/**

 * @brief Class for reading and storing the node's settings.
 */
class PCunpackerSettings{
    public:

        
        std::string input_processed_clouds; //!< the (packed) processed cloud
        std::string output_surface;         //!< topic for publishing the surface part to
        std::string output_edge ;           //!< topif for publishing the edge part to
    
        /**
         * @brief Read the node settings from the private node handle.
         * @param privateNh private node handle
         * @return true if reading was successfull, else false
         */
        bool readSettings(ros::NodeHandle& privateNh){
            
            // get topics
            privateNh.param("input_processed_clouds", input_processed_clouds, std::string("processed_clouds"));
            privateNh.param("output_surface",         output_surface,         std::string("surface"));
            privateNh.param("output_edge",            output_edge,            std::string("edge"));
            
            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;
        }
        
        /**
         * @brief Print a nice help message about how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for PCunpackerNodelet\n"
                << "-------------------------------------\n"
                << "This nodelet publishes the clouds of a ProcessedClouds message individually.\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "input_processed_clouds: topic of the original processed cloud\n"
                << "output_surface:         topic to publish the surface part to\n"
                << "output_edge:            topic to publish the edge part to\n"
                << "\n"
                << "(To hide this message, start the node / nodelet with _help:=false)"
                );
        }
};




/**
 * @class PCunpackerNodelet
 * @brief Nodelet for unpacking and republishing a processd clouds message
 */
class PCunpackerNodelet : public nodelet::Nodelet{
    
    public:
        
    private:
    
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters
            
            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }
            
            // publishers
            pubSurface = nh.advertise<sensor_msgs::PointCloud2>(settings.output_surface, 10, true);
            pubEdge =    nh.advertise<sensor_msgs::PointCloud2>(settings.output_edge, 10, true);
            if (!pubSurface || !pubEdge){
                NODELET_ERROR_STREAM("nh.advertise returned an empty publisher. Shutting down.");
                exit(EXIT_FAILURE);
            }

            // subscriber for original point cloud
            subPC = nh.subscribe(settings.input_processed_clouds, 10,
                                 &PCunpackerNodelet::unpackAndPublish, this);
        }
        
        /**
         * @brief take a message with processed point clouds and republish them separately
         * @param msg_in the message
         */
        void unpackAndPublish(const pf_matching_core::ProcessedClouds::ConstPtr& msg_in){
            
            pubSurface.publish(msg_in->surface);
            pubEdge.publish(msg_in->edges);

        }
   
    // ROS related
    ros::Publisher pubSurface; //!< publisher for surface part
    ros::Publisher pubEdge;    //!< publisher for surface part
    ros::Subscriber subPC;  //!< subscriber for the processed clouds message
    
    // settings
    PCunpackerSettings settings; //!< topic settings
};

} // end namespace
} // end namespace


// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::PCunpackerNodelet, nodelet::Nodelet)