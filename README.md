This package contains additional nodelets and launch files to facilitate
in- and output to the nodelets of the pf_matching_core package and tools to
visualize intermediate results.


# INSTALLATION

The package was tested with ROS Indigo on Ubuntu 14.04 and with PCL 1.71.
To install the package, please follow the steps below.
It is assumed that you are using a catkin
workspace that is located at `~/ROS/catkin_worksace` and that all dependencies
were installed as described in the pf_matching_core package.

1. (install pf_matching_core package)

2. Download this Git project into the source folder:
`
mkdir -p ~/ROS/catkin_workspace/src/pf_matching_tools
cd ~/ROS/catkin_workspace/src/pf_matching_tools
git clone https://gitlab.tubit.tu-berlin.de/Master_thesis_Xaver/pf_matching_tools.git .
`
4. Build the catkin workspace and source the changes:
`
cd ~/ROS/catkin_workspace
catkin_make
source ~/ROS/catkin_workspace/devel/setup.bash
rospack profile
`

# CONTENTS

## NODES

The package provides the following nodes:
* `pcLoader` to load point clouds from files and publish them as PointCloud2-messages
* `pcViewer` to visually inspect PointCloud2-messages
* `pcNoisifier` to add artificial noise to PointCloud2-messages
* `pcPacker` to pack two PointCloud2-messages as edge and surface clouds into a
             ProcessdClouds-message as input to the matcher

* `pcUnpacker` to unpack a ProcessdClouds-message to two PointCloud2-messages,
               for example for inspection

From a sourced terminal, start them via `rosrun pf_matching_tools XY`.

Any algorithm settings are available through the dynamic_reconfigure interface
(`rosrun rqt_reconfigure rqt_reconfigure`). For available services and the
configuration of the in- and output topics, please start the nodes without
arguments to display a help message.

All nodes are also available as nodelets.

## LAUNCH FILES

* `cloud_processing_test.launch` contains a pipeline to load a point cloud,
                                 process its contents, and display the results.
* `noisify_processed_clouds_as_nodes.launch` adds noise to a ProcessedClouds-message.
* `pipeline_as_nodes.launch` is a full pipeline for experiments with data input from file,
                             pre-processing of models and scenes, and matching. Note that
                             this launch file does not configure the pipeline in any way.
                             This has to be done through the dynamic_reconfigure interface
                             by the experiment or manually.
* `pipeline_noisy_after_preprocessing.launch` is the same as `pipeline_as_nodes.launch`, but contains an additional
                                              node to add noise to pre-processed point clouds (legacy code for an old experiment).
* `pipeline_noisy_as_nodes.launch` is the same as `pipeline_as_nodes.launch`, but contains an additional node to simulate
                                    sensor noise for the scene point cloud before pre-processing.
* `show_processed_clouds.launch` unpacks the contents of a ProcessedClouds-message
                                 and displays them. For example, run `roslaunch pf_matching_tools show_processed_clouds.launch processed_cloud_in:=/scene_clouds` to display the
                                 processed scene clouds of a pipeline that was started through `pipeline_as_nodes.launch`.

Note that all pipelines contain only nodes to make debugging and understanding them easier.
To achieve higher overall speeds, the same pipelines could also be built with nodelets.

# MISC

* A documentation of the source code can be built via Doxygen:
`
cd ~/ROS/catkin_workspace/src/pf_matching_tools
doxygen
`
The documentation will be located under `/doc`.
