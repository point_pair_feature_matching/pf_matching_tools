/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 09/10/15
 * @file pcNoisifier.hpp
 * @brief Functions and classes related to adding artificial noise to point clouds.
 */

#pragma once

// PCL
#include <pcl/point_cloud.h>

namespace pf_matching{
namespace pre_processing{
    
/**
 * @class PCnoisifier
 * @brief Class for adding zero mean Gaussian white noise to a point cloud.
 * 
 */
class PCnoisifier{
    
    public:
        /**
         * possible modes for noisifying a cloud
         */
        enum NoisificationMethod{
            NONE = 0,      //!< no noisification, keeps cloud's organization intact
            ANY_DIRECTION, //!< move points random amount in random direction,
                           //   destroys cloud's organization
            DEPTH          //!< move points random amount in direction of cloud's sensor origin
                           //   (simulate depth noise), keeps cloud's organization intact
        };
        
        /**
         * @brief Set the random seed to a new value.
         * @param randomSeed_ Seed for the random engine.
         */
        void setSeed(unsigned randomSeed_){
            randomSeed = randomSeed_;
        }
        
        /**
         * @brief set the noisification method
         * @param method_ method to use
         */
        void setMethod(NoisificationMethod method_){
            method = method_;
        }
        
        /**
         * @brief add zero mean Gaussian white noise to a point cloud
         * 
         * @param[in,out] cloudPtr the cloud to add noise to
         * @param[in] noiseSD standard deviation
         * @param[in] sdIsRelative if true, noiseSD is relative to the object diameter (i.e. the
         *        the diameter of the minimal bounding sphere around the cloud)
         * @return the absolute standard deviation used for the noise (equals noiseSD if 
         *         sdIsRelative == false)
         */
        template <typename PointT> float
        noisifyPC(typename pcl::PointCloud<PointT>::Ptr& cloudPtr,
                  float noiseSD,
                  bool sdIsRelative = false);
        
    private:
        unsigned randomSeed = 1;
        NoisificationMethod method = NONE;
        std::default_random_engine randEngine;
};

} // end namespace
} // end namespace

#include "./impl/pcNoisifierImpl.hpp"