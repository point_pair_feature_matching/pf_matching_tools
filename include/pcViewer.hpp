/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 16/11/15
 * @file pcViewer.hpp
 * @brief Functions and classes for quickly displaying a point cloud and normals.
 */

#pragma once

// other
#include <string>

// PCL
#define VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl/visualization/pcl_visualizer.h>


using namespace pcl::visualization;

namespace pf_matching{
namespace visualization{
    
/**
 * @class PCviewer
 * @brief Class for displaying a point cloud and surface normals.
 * 
 */
class PCviewer{
    
    public:
        /**
         * @brief Constructor
         * @param windowName title to appear at top of display windows
         * @param bgR r -channel of the window's background color
         * @param bgG g -channel of the window's background color
         * @param bgB b -channel of the window's background color
         */
        PCviewer(std::string windowName = "Point Cloud viewer",
                 float bgR = 0.5, float bgG = 0.5, float bgB = 0.5);

        
        /**
         * @brief add a point cloud to the viewer and display its points
         * @param cloudConstPtr the cloud to display
         * @param name t display for the cloud
         * @param r red color value [0-255]
         * @param g green red color value [0-255]
         * @param b blue red color value [0-255]
         * @param pointSize display size of points
         * @return true if cloud was added, false if cloud was only updated
         */
        template <typename PointT> bool
        addCloud(const typename pcl::PointCloud<PointT>::ConstPtr cloudConstPtr,
                 std::string name,
                 int r = 255, int g = 0, int b = 0,
                 int pointSize = 1);

        /**
         * @brief add a point cloud to the viewer and display its points and normal vectors
         * @param cloudConstPtr the cloud to display
         * @param name t display for the cloud
         * @param r red color value [0-255]
         * @param g green red color value [0-255]
         * @param b blue red color value [0-255]
         * @param pointSize display size of points
         * @return true if cloud was added, false if cloud was only updated
         */
        template <typename PointT> void
        addCloudNormal(const typename pcl::PointCloud<PointT>::ConstPtr cloudConstPtr,
                       std::string name,
                       int r = 255, int g = 0, int b = 0,
                       int pointSize = 1);
        
        
        
        /**
         * @brief template instantiation for convenience
         */
        bool addCloud(const typename pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloudConstPtr,
                      std::string name,
                      int r = 255, int g = 0, int b = 0,
                      int pointSize = 1){
            return addCloud<pcl::PointXYZ>(cloudConstPtr, name, r, g, b, pointSize);
        }
        
        /**
         * @brief template instantiation for convenience
         */
        bool addCloud(const pcl::PointCloud<pcl::PointNormal>::ConstPtr cloudConstPtr,
                      std::string name,
                      int r = 255, int g = 0, int b = 0,
                      int pointSize = 1){
            return addCloud<pcl::PointNormal>(cloudConstPtr, name, r, g, b, pointSize);
        }
        
        /**
         * @brief template instantiation for convenience
         */
        void addCloudNormal(const pcl::PointCloud<pcl::PointNormal>::ConstPtr cloudConstPtr,
                            std::string name,
                            int r = 255, int g = 0, int b = 0,
                            int pointSize = 1){

            addCloudNormal<pcl::PointNormal>(cloudConstPtr, name, r, g, b, pointSize);
        }
        
        
        /**
         * @brief display the coordinate system for the given point cloud
         * @param cloudConstPtr the cloud
         * @param name text to display
         * @param scale size of coordinate system, if 0, will be 25% of cloud diameter
         */
        template <typename PointT> void
        addCoordinateSystem(const typename pcl::PointCloud<PointT>::ConstPtr cloudConstPtr,
                            std::string name,
                            float scale = 0.0);
        
        /**
         * @brief display a coordinate system at the origin
         * @param scale size of the coordinate system
         */
        void addOrigin(float scale = 1.0);
        
        
        /**
         * @brief display the data until the window is closed
         */
        void spinContinous();
        
        /**
         * @brief process all queued callbacks
         * @param time max. time of the visualization loop in ms
         * @param force_redraw if true, redraw everything
         */
        void spinOnce(int time = 1, bool force_redraw = false);
        
        /**
         * @brief check if the visualization was closed
         * @return the status
         */
        bool wasStopped();
        
    public:
        pcl::visualization::PCLVisualizer visualizer; //!< the employed pcl visualization object
        
    private:
        
        int nClouds = 0; // number of clouds displayed
        int fontsize = 15;
        
};

} // end namespace
} // end namespace

#include "./impl/pcViewerImpl.hpp"