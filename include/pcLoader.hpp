/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 09/10/15
 * @file pcLoader.hpp
 * @brief Functions and classes related to loading a file into PCL and perform basic operations
 *        to make it usable.
 */
#pragma once

// PCL
#include <pcl/point_types.h>
#include <pcl/point_cloud.h> //PointCloud template


namespace pf_matching{
namespace io{


/**
 * @class PCloader
 * @brief Class for loading point cloud data and meshed models via PCL.
 * 
 * The information loaded from the file will depend on the passed point cloud type. You might need
 * to change data field names to match the names expected by the underlying loader functions. 
 * E.g. change 'nx' to 'normal_x' to load normal information.
 */
template<typename PointT>
class PCloader{
    public:
        /**
         * @brief constructor
         * @param pointCloudPtr_ shared pointer to the point cloud to load the data into
         */
        PCloader(typename pcl::PointCloud<PointT>::Ptr pointCloudPtr_);

        /**
         * @brief Loads a point cloud from the file specified.
         * 
         * The file type is determined by the file extension. Any information about the sensor 
         * orientation and origin is set to identity and [0 0 0 0] respectively if the sensor
         * orientation is inconsistent (because of bad file or PCL-bug for PLYloader in PCL 1.7).
         *
         * @param path file path of the point cloud.
         * @return true for success, else false
         */
        bool loadCloud(std::string path);

        /**
         * @brief Recenter the loaded point cloud so that the origin of the coordinate
         *        frame is at the cloud's center of mass (= centroid).
         * @note This only moves the point cloud and NOT the sensor origin! Do not use this function
         *       on data if you intend to use the original sensor origin for later processing (e.g.
         *       for flipping normals).
         * @return translation vector to the centeroid before recentering (adding this vector to
         *         the recentered cloud will restore the original cloud)
         */
        Eigen::Vector3f recenterCloud();
        
        /**
         * @brief Scale the xyz-dimensions of the loaded point cloud to e.g. convert different
         *        units at recording time. The scaling should be done so that the node outputs
         *        SI units (i.e. meters)
         * @param scaleFactor Factor to multiply xyz-dimensions with.
         * @note  This scales both xyz-data AND the position of the sensor origin to ensure consistency.
         */
        void scaleCloud(float scaleFactor);

        /**
         * @brief Get a reference to the point cloud used for storage
         * @return pointer to the cloud
         */
        typename pcl::PointCloud<PointT>::Ptr getCloudPtr();

    private:
        // shared pointer to the point cloud to load model into
        typename pcl::PointCloud<PointT>::Ptr pointCloudPtr = NULL;
};

} // end namespace
} // end namespace

#include "impl/pcLoaderImpl.hpp"