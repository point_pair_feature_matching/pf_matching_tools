/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../pcViewer.hpp"

#include <pcl_conversions/pcl_conversions.h>
#include <boundingSphere.hpp>

namespace pf_matching{
namespace visualization{



PCviewer::PCviewer(std::string windowName, float bgR, float bgG, float bgB){
    visualizer.setBackgroundColor (bgR, bgG, bgB);
    visualizer.setWindowName(windowName);
}


template <typename PointT> bool
PCviewer::addCloud(const typename pcl::PointCloud<PointT>::ConstPtr cloudConstPtr,
                   std::string name, int r, int g, int b, int pointSize){
    
    // check whether point cloud already exists, note: from PCL 1.8 on, we can use contains() here
    bool pcExists = visualizer.updatePointCloud<PointT>(cloudConstPtr, name);

    if(!pcExists){
        // adding a totally new point cloud
        nClouds++;
        visualizer.addText(name, 0, floor(fontsize * 1.2) * (nClouds - 1) + 10, fontsize, r/255.0, g/255.0, b/255.0, name + "_text");
        //PCL_INFO("[PCviewer::addCloud] Adding cloud.\n");
    }
    else{
        // updating, remove old point cloud with that name
        visualizer.removePointCloud(name);
        visualizer.removePointCloud(name + "_normals");
    }
    
    PointCloudColorHandlerCustom<PointT> cloudColor(cloudConstPtr, r, g, b);
    visualizer.addPointCloud(cloudConstPtr, cloudColor, name);
    visualizer.setPointCloudRenderingProperties (PCL_VISUALIZER_POINT_SIZE, pointSize, name);
    
    
    visualizer.resetCamera();
    
    if(!pcExists){
        return true;
    }
    else{
        return false;
    }
    
}


template <typename PointT> void
PCviewer::addCloudNormal(const typename pcl::PointCloud<PointT>::ConstPtr cloudConstPtr,
                   std::string name, int r, int g, int b, int pointSize){

    std::string normalsName = name + "_normals";
    Eigen::Vector4f boundingSphere;
    pf_matching::tools::epos6BoundingSphere(*cloudConstPtr, boundingSphere);
    
    // add points and check whether that was an update or an actual add
    if(addCloud<PointT>(cloudConstPtr, name, r, g, b, pointSize)){
        // added new cloud
    }
    else{
        // updated existing cloud
        std::cout << "removing old normals cloud\n";
        visualizer.removePointCloud(normalsName);
    }
    visualizer.addPointCloudNormals<PointT>(cloudConstPtr, 1, 0.02 * 2*boundingSphere[3], normalsName , 0);
    visualizer.setPointCloudRenderingProperties (PCL_VISUALIZER_COLOR, r/255.0, g/255.0, b/255.0, normalsName);
}



template <typename PointT> void
PCviewer::addCoordinateSystem(const typename pcl::PointCloud<PointT>::ConstPtr cloudConstPtr,
                    std::string name, float scale){

    Eigen::Affine3f coordinateSystemTf = Eigen::Affine3f::Identity();
    Eigen::Vector4f t4f = cloudConstPtr->sensor_origin_;
    coordinateSystemTf.translate(t4f.head<3>()); // .head returns first n elements of vector, needed to downsize from Vector4f to Vector3f
    coordinateSystemTf.rotate(cloudConstPtr->sensor_orientation_);
    
    if(scale == 0.0){

        Eigen::Vector4f boundingSphere;
        pf_matching::tools::epos6BoundingSphere(*cloudConstPtr, boundingSphere);
        
        scale = 0.25 * 2 * boundingSphere[3];
    }

        visualizer.addCoordinateSystem(scale, coordinateSystemTf);
        visualizer.addText3D(name, pcl::PointXYZ(t4f[0], t4f[1], t4f[2]), scale / 10, 0, 0, 0);
}

void PCviewer::addOrigin(float scale){
    visualizer.addCoordinateSystem(scale);
    visualizer.addText3D("origin", pcl::PointXYZ(0, 0, 0), scale / 10, 0, 0, 0);
}


void PCviewer::spinContinous(){
    while (!visualizer.wasStopped ()) { 
        visualizer.spinOnce ();
    }
}


void PCviewer::spinOnce(int time, bool force_redraw){
    visualizer.spinOnce(time, force_redraw);
}


bool PCviewer::wasStopped(){
    return visualizer.wasStopped();
}

} // end namespace
} // end namespace