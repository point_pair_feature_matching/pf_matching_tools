/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

/**
 * @author Xaver Kroischke
 * @date 16/11/15
 * @file pcLoader.hpp
 * @brief Implementation of pcLoader.
 */
 #pragma once

// project
#include "../pcLoader.hpp"

// PCL
#include <pcl/io/ply_io.h>   // for loading .ply files
#include <pcl/io/pcd_io.h>   // for loading .pcd files
#define VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl/io/vtk_lib_io.h>     // for loading mesh files like .vkt, .obj, .stl
#include <pcl/common/transforms.h> // for scaling of the point cloud
#include <pcl/common/centroid.h>   // for estimation of point cloud centroid and demeaning

// standard libs
#include <regex> // for checking file extensions

/**
 * @brief Check whether a given file path has a certain file extension
 * @param path file path to check
 * @param ending file ending without "." (e.g. "ply" for .ply-files)
 * @return true if path has the ending, else false
 */
static bool checkExtension(std::string path, std::string ending){
    std::regex e("(.*)(\\." + ending + ")");
    return std::regex_match(path, e,std::regex_constants::ECMAScript);
}

namespace pf_matching{
namespace io{

    template<typename PointT>
    PCloader<PointT>::PCloader(typename pcl::PointCloud<PointT>::Ptr pointCloudPtr_){
        
        // save pointers internally
        pointCloudPtr = pointCloudPtr_;
    }

    template<typename PointT>
    bool PCloader<PointT>::loadCloud(std::string path){

        // erase any pervious contents
        pointCloudPtr->clear();
        
        // check if file does not exists (taken from http://stackoverflow.com/a/12774387)
        if(access(path.c_str(), F_OK) == -1){
            PCL_ERROR("File '%s' does not exist.\n", path.c_str());
            return false;
        }
        
        // variable to check for any errors during load
        bool load_success;
        
        // just info
        PCL_INFO("Trying to load file '%s'.\n", path.c_str());

        // suppress warning messages of loaders via verbosity level
        auto oldVerbosityLevel = pcl::console::getVerbosityLevel();
        pcl::console::setVerbosityLevel(pcl::console::L_ERROR);
        
        // load file according to file extension
        if(checkExtension(path,"pcd")){
            // load .pcd file
            load_success = !pcl::io::loadPCDFile(path, *pointCloudPtr); // returns 0 at success
        }
        else if(checkExtension(path,"ply")){
            // load .ply file
            load_success = !pcl::io::loadPLYFile(path, *pointCloudPtr); // returns 0 at success
        }
        else if(checkExtension(path,"stl") || checkExtension(path,"vtk") || checkExtension(path,"obj")){
            // load a mesh file
            
            // load into polygon representation
            pcl::PolygonMesh mesh;
            load_success = 0 < pcl::io::loadPolygonFile(path, mesh); // returns loaded vertices
            
            // convert to point cloud
            pcl::fromPCLPointCloud2(mesh.cloud, *pointCloudPtr);
        }
        else{
            // file extension is unknown
            PCL_ERROR("File format not supported for loading.\n");
            load_success = false;
        }
        
        // reset verbosity level
        pcl::console::setVerbosityLevel(oldVerbosityLevel);
        
        // if loading failed, we can stop here
        if(!load_success){
            return false;
        }
        
        // Set the sensor orientation and position to the identity if a inconsistent orientation
        // is given for the sensor. Usually caused by PCL bug, see:
        // https://github.com/PointCloudLibrary/pcl/pull/879
        // https://github.com/PointCloudLibrary/pcl/issues/626
        if(!pointCloudPtr->sensor_orientation_.isApprox(pointCloudPtr->sensor_orientation_.normalized(),
            0.0001f)){
            PCL_INFO("Sensor orientiation quaternion not normalized. Replacing by identity.\n");
            PCL_DEBUG("old orientation quaternion (xyzw): (%f, %f, %f, %f)\n",
                      (*pointCloudPtr).sensor_orientation_.x(),
                      (*pointCloudPtr).sensor_orientation_.y(),
                      (*pointCloudPtr).sensor_orientation_.z(),
                      (*pointCloudPtr).sensor_orientation_.w());
                      
            (*pointCloudPtr).sensor_orientation_.setIdentity();
            (*pointCloudPtr).sensor_origin_ = Eigen::Vector4f(0, 0, 0, 0);
        }
        
        
        // info
        PCL_INFO("Loaded %d points.\n", pointCloudPtr->width * pointCloudPtr->height);

        return true;
    }

    template<typename PointT>
    Eigen::Vector3f PCloader<PointT>::recenterCloud(){
        PCL_DEBUG("Recentering point cloud.\n");
        
        // calculate the centroid of the points
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*pointCloudPtr, centroid);
        
        // substract the centroid from the point cloud
        pcl::demeanPointCloud(*pointCloudPtr, centroid, *pointCloudPtr);
        
        // return centeroid as 3-component (xyz) vector
        return centroid.head<3>();
    }

    template<typename PointT>
    void PCloader<PointT>::scaleCloud(float scaleFactor){
        // exclude scaling values <= 0
        if(scaleFactor <= 0){
            PCL_ERROR("Scaling factor has to be > 0. Scaling skipped.\n");
            return;
        }

        // construct homogeneous transformation matrix for uniform scaling
        Eigen::Affine3f scaleTransform = Eigen::Affine3f::Identity();
        scaleTransform.scale(scaleFactor);
        
        // apply transformation matrix to point cloud and the sensor origin
        pcl::transformPointCloud(*pointCloudPtr, *pointCloudPtr, scaleTransform);
        pointCloudPtr->sensor_origin_ = scaleTransform * pointCloudPtr->sensor_origin_;
        
    }

    template<typename PointT>
    typename pcl::PointCloud<PointT>::Ptr PCloader<PointT>::getCloudPtr(){
        return pointCloudPtr;
    }

} // end namespace
} // end namespace
