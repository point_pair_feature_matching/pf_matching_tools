/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../pcNoisifier.hpp"

// other
#include <string>
#include <random>

// project
#include <boundingSphere.hpp>

namespace pf_matching{
namespace pre_processing{


template <typename PointT> float
PCnoisifier::noisifyPC(typename pcl::PointCloud<PointT>::Ptr& cloudPtr,
                       float noiseSD,
                       bool sdIsRelative){
              
    // if the standard deviation is exactly 0 or there are no points in the cloud, do nothing
    if(noiseSD == 0 || cloudPtr->points.size() == 0){
        return 0.0;
    }
    
    // determine value for standard deviation
    float sd;
    if(sdIsRelative){
        // standarad deviation to use is calculated with model diameter
        
        // calculate model diameter via minimal bounding sphere
        Eigen::Vector4f boundingSphere; // entry 3 will be radius
        pf_matching::tools::welzlBoundingSphere(*cloudPtr, boundingSphere);
        float cloudDiameter = 2 * boundingSphere[3];
        
        // set standard deviation
        sd = cloudDiameter * noiseSD;
    }
    else{
        // use absolute standard deviation
        sd = noiseSD;
    }

    // reset all random-related objects, so that subsequent function calls with the same
    // seed produce the same output on identical clouds
    std::normal_distribution<float> distribution(0.0, sd); // default mean is 0
    randEngine.seed(randomSeed);
    
    switch(method){
        case NONE:
            // Do nothing.
            break;
            
        case ANY_DIRECTION:
            // add noise to every point's coordinates in random direction
            for(typename pcl::PointCloud<PointT>::iterator point = cloudPtr->begin();
                point!= cloudPtr->end();
                point++){
                
                // construct a unit vector of random direction
                // using a NORMAL distribution for xyz will give a UNIFORM distribution of
                // the vector direction (see http://stackoverflow.com/a/9751925)
                Eigen::Vector3f v(distribution(randEngine),
                                  distribution(randEngine),
                                  distribution(randEngine));
                v.normalize();
                
                // set the magnitude of the vector to a random value according to the
                // nosification settings
                v *= distribution(randEngine);
                
                // add the noise to the points
                point->x += v.x();
                point->y += v.y();
                point->z += v.z();
            }
            
            // choosing any direction will destroy any organization the cloud had
            cloudPtr->width *= cloudPtr->height;
            cloudPtr->height = 1;
            
            break;
            
        case DEPTH:
        
            // get the location of the sensor origin
            const Eigen::Vector4f sOrigin = cloudPtr->sensor_origin_;
            
            // add noise to every point along it's connecting line with the cloud's sensor
            // origin
            for(typename pcl::PointCloud<PointT>::iterator point = cloudPtr->begin();
                point!= cloudPtr->end();
                point++){
                    
                // calculate unit vector pointing from sOrigin to the current point
                Eigen::Vector3f v = point->getVector3fMap() - sOrigin.head<3>();
                v.normalize();
                
                // set the magnitude of the vector to a random value according to the
                // nosification settings
                v *= distribution(randEngine);
                
                // add the noise to the points
                point->x += v.x();
                point->y += v.y();
                point->z += v.z();
                }

            break;
    }
    
    return sd;

}


} // end namespace
} // end namespace